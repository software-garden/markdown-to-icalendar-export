This document is used for integration tests. It will be passed to the `md-to-ics` program and the output will be compared with the `expected.ics` file. If you modify the `expected.ics` file, please remember to make sure it's valid using [the iCalendar validator][].

# Simple Sample

Anything outside the list will be ignored.

- <on:2022-05-12> Whole day event

- <on:2022-05-13T12:30> Afternoon thing

  With details.
  
  
- <on:2022-06-01T17:00> Meet the king

  Nieuwezijds Voorburg 147
  1012RJ Amsterdam
  Netherlands


# Another list, just for fun

- <on:2022-03-01> I did nothing
- **[Tuesday evening](on:2022-04-19)** was *boring*
- If it doesn't start on <on:2023-05-20> it's too late.
- Nested list 
  - <on:2022-03-05> will
  - [be ignored](on:2022-03-06) too.

- <on:2022-06-05/P3D> All day from June 5th, 2022 until June 8th, 2022

- <on:2022-06-05T16:00/PT2H> On June 5th, 2022, from 16:00 until 18:00


[the iCalendar validator]: https://icalendar.org/validator.html
