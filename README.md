![Markdown to iCalendar Export Icon](icon.svg) Export schedule from a markdown document to the iCalendar (ICS) format


# Use

``` shell
cat README.md | md-to-ics --timezone=Europe/Amsterdam > calendar.ics
```

Here's an example schedule:

``` markdown
# The Master Plan

- <on:2022-05-08T23:30> Get something done
- <on:2022-05-09> Be happy
- <on:2022-05-10> Do something elaborate

  First, open your eyes. Yawn.
  
  Snooze and repeat.
  
- [Later that day](2022-05-10T12:30) Take a nap 
```


# What is it for

At Software Garden we use a process called _mob planning_. Every Tuesday the team connects over a video call and we review the past week and plan the next 10 days. We use a single markdown file synchronized using Git. You can read more about it at <https://software.garden/mob-planning>.

This tool enhances our process by providing an interface for calendaring apps. Since our plan is already in a Git repository it's easy to run md-to-ics in a continuous integration pipeline and publish the .ics file on the web. Any team member can subscribe to this file and see planned events in their own calendar.


# Syntax

We don't have a formal definition yet as we experiment with the designs, looking for one that will best support our workflow. 

Overview:

``` markdown
- <on:YYYY-MM-DD> Summary of a whole-day event

  Description of the event.
  
  Can be multi line.
  
- [Labe]()
```

Here are some informal rules.

1. Events are extracted from items of top-level lists in the markdown document

   Only the top-level, i.e. nested lists or lists within tables, etc are not processed.
   
2. Only items that _start_ with a time-link are treated as items

   Time link is a link `on` schema. The path has to be a valid ISO8601 date-time string. The time part is optional. Only the `href` of a link is considered, the label is not significant. These are example of such links: 
   
   1. `<on:2022-05-30>`
   2. `[Monday](on:2022-05-30)`
   3. `[Later today](on:2022-05-30T17:30)`
   
3. The link can be nested in an inline like `strong` or `em`, but all it's parents up to the list item need to be first children. For example:
 

   1. `- <on:2022-03-14> food delivery`
   2. `- [Monday](on:2022-03-14) is the best day`
   3. `- **[Monday](on:2022-03-14)** is the very best day`
   
   But not
   
   1. `- On [Monday](on:2022-03-14) we'll get the food.`
   2. `- Do something before <on:2022-04-16>.`
   
4. The first line of the item becomes the summary.

   If the label of the link is the same as the URI (usually because it's auto-link like `<on:2022-05-30>`) it will be removed from the summary.
   
   For example:
   
   ``` markdown 
   - **<on:2022-04-15> run** to the shop.
   ```
   
   Will become:
   
   ``` text
   run to the shop.
   ```
   
   If the label is different, the link will be 

5. The subsequent paragraphs of the item will become the description


# Build

Best with Nix:

``` shell
nix build
```

The executable will be at `result/bin/`

Alternatively (or during development) use Cargo directly:

``` shell
cargo build
```

> TODO: Setup a Makefile, but make sure it works well with Nix - we don't want two slightly different build systems to maintain.


# Design principles


1. Simplicity

   Simple command line interface following the Unix philosophy.
   

2. Pragmatism

   We build it to scratch our itch, not as a general calendaring solution. We will only develop features necessary to support solid, well defined use cases and only if there are no simple work-arounds.


3. Following standards
  
   The expected input should be a valid, standard markdown that can be reasonably processed by other markdown tools. Specifically the time-links should use standard URIs so they are always rendered as links.
   
   The output should be a standard iCalendar text as described in [the RFC 5545](https://datatracker.ietf.org/doc/html/rfc5545). 
   
   
4. Optimize for ease of editing and reading

   The time links URI has to be straight-forward to read and write.
   

5. Single source of truth

   The markdown file(s) should be the single source of truth. That specifically excludes any stateful solutions.

   
6. Reasonably cross-platform

   Esp. in regards to different calendaring applications.



# TODO

- [ ] Unit tests
- [x] Integration test
- [x] Library
- [ ] Rationale
