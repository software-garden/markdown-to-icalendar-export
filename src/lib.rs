extern crate chrono;
extern crate chrono_tz;
extern crate sha2;
extern crate url;

pub mod duration {
    use std::fmt::Display;
    use std::str::FromStr;


    use nom::{
        bytes::complete::tag,
        character::complete::digit1,
        combinator::{all_consuming, map_res, opt},
        error::{ErrorKind, ParseError},
        sequence::{preceded, terminated, tuple},
        Err, IResult,
    };

    #[derive(Debug, PartialEq, Clone, Copy)]
    pub struct Duration {
        pub days: Option<i32>,
        pub hours: Option<i32>,
        pub minutes: Option<i32>,
        pub seconds: Option<i32>,
    }

    impl Duration {
        pub fn parse(input: &str) -> Result<Duration, Err<(&str, ErrorKind)>> {
            let (_, duration) = all_consuming(preceded(
                tag("P"),
                //alt((parse_week_format, parse_basic_format)),
                parse_basic_format,
            ))(input)?;

            Ok(duration)
        }
    }

    impl Display for Duration {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            let days = self.days.map_or(String::new(), |d| d.to_string() + &"D".to_string());
            let hours = self.hours.map_or(String::new(), |h| h.to_string() + &"H".to_string());
            let minutes = self.minutes.map_or(String::new(), |m| m.to_string() + &"M".to_string());
            let seconds = self.seconds.map_or(String::new(), |s| s.to_string() + &"S".to_string());
            let time = hours + &minutes + &seconds;
            let full_time = match time.is_empty() {
                true => String::new(),
                false => "T".to_string() + &time,
            };
            write!(f, "{}", "P".to_string() + &days + &full_time)
        }
    }

    fn parse_basic_format(input: &str) -> IResult<&str, Duration> {
        let (input, day) = opt(value_with_designator("D"))(input)?;

        let (input, time) = opt(preceded(
            tag("T"),
            tuple((
                opt(value_with_designator("H")),
                opt(value_with_designator("M")),
                opt(value_with_designator("S")),
            )),
        ))(input)?;

        let (hour, minute, second) = time.unwrap_or_default();

        if day.is_none()
            && hour.is_none()
            && minute.is_none()
            && second.is_none()
        {
            Err(Err::Error(ParseError::from_error_kind(
                input,
                ErrorKind::Verify,
            )))
        } else {
            Ok((
                input,
                Duration {
                    days: day,
                    hours: hour,
                    minutes: minute,
                    seconds: second,
                },
            ))
        }
    }

    fn value_with_designator(designator: &str) -> impl Fn(&str) -> IResult<&str, i32> + '_ {
        move |input| {
            terminated(
                map_res(digit1, |s: &str| FromStr::from_str(s)),
                tag(designator),
            )(input)
        }
    }

//    fn parse_week_format(input: &str) -> IResult<&str, Duration> {
//        let (input, week) = value_with_designator("W")(input)?;
//
//        Ok((
//            input,
//            Duration {
//                year: 0.,
//                month: 0.,
//                day: week * 7.,
//                hour: 0.,
//                minute: 0.,
//                second: 0.,
//            },
//        ))
//    }
}

pub mod calendar {
    use std::fmt::Display;

    use crate::duration::Duration;
    use chrono::{NaiveDate, NaiveDateTime};
    use chrono_tz::Tz;
    use hex;
    use sha2::Digest;

    pub struct Calendar {
        pub name: String,
        pub product: String,
        pub timezone: Tz,
        pub items: Vec<Item>,
    }

    pub enum Item {
        Event {
            start: NaiveDateTime,
            maybe_duration: Option<Duration>,
            summary: String,
            description: String,
        },
        WholeDayEvent {
            start: NaiveDate,
            maybe_duration: Option<Duration>,
            summary: String,
            description: String,
        },
    }

    impl Calendar {
        pub fn add_item(&mut self, item: Item) -> &Self {
            self.items.push(item);
            self
        }
    }

    impl Display for Calendar {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            writeln!(f, "BEGIN:VCALENDAR")?;
            writeln!(f, "PRODID:{}", self.product)?;
            writeln!(f, "CALSCALE:GREGORIAN")?;
            writeln!(f, "VERSION:2.0")?;
            writeln!(f, "METHOD:PUBLISH")?;
            writeln!(f, "X-WR-CALNAME:{}", self.name)?;
            writeln!(f, "X-WR-TIMEZONE:{}", self.timezone)?;
            for item in &self.items {
                writeln!(f, "BEGIN:VEVENT")?;
                match item {
                    Item::Event {
                        start,
                        maybe_duration,
                        summary,
                        description,
                    } => {
                        let mut hasher = sha2::Sha256::new();
                        hasher.update(format!("{} {}", start, summary));
                        let uid = hex::encode(hasher.finalize());

                        writeln!(f, "DTSTART:{}", start.format("%Y%m%dT%H%M%S"))?;
                        if let Some(duration) = maybe_duration {
                            writeln!(f, "DURATION:{duration}")?;                            
                        }
                        writeln!(f, "SUMMARY:{}", summary)?;
                        writeln!(f, "DESCRIPTION:{}", description)?;
                        writeln!(f, "DTSTAMP:{}", "19700101T000000Z")?;
                        writeln!(f, "UID:{}", uid)?;
                    }
                    Item::WholeDayEvent {
                        start,
                        maybe_duration,
                        summary,
                        description,
                    } => {
                        let mut hasher = sha2::Sha256::new();
                        hasher.update(format!("{} {}", start, summary));
                        let uid = hex::encode(hasher.finalize());

                        writeln!(f, "DTSTART;VALUE=DATE:{}", start.format("%Y%m%d"))?;
                        if let Some(duration) = maybe_duration {
                            writeln!(f, "DURATION:{duration}")?;                            
                        }
                        writeln!(f, "SUMMARY:{}", summary)?;
                        writeln!(f, "DESCRIPTION:{}", description)?;
                        writeln!(f, "DTSTAMP:{}", "19700101T000000Z")?;
                        writeln!(f, "UID:{}", uid)?;
                    }
                }
                writeln!(f, "END:VEVENT")?;
            }
            writeln!(f, "END:VCALENDAR")
        }
    }
}

pub mod converter {
    use std::fmt::Display;

    use crate::calendar::{self, Calendar};
    use crate::duration::Duration;
    use chrono::prelude::*;
    use pulldown_cmark;
    use url::Url;

    #[derive(PartialEq, Clone, Copy)]
    enum TimeUri {
        Time(NaiveDateTime),
        Date(NaiveDate),
        TimeDuration(NaiveDateTime, Duration),
        DateDuration(NaiveDate, Duration),
    }

    impl Display for TimeUri {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                TimeUri::Time(time) => write!(f, "on:{}", time.format("%Y-%m-%dT%H:%M")),
                TimeUri::Date(date) => write!(f, "on:{date}"),
                TimeUri::TimeDuration(time, duration) => write!(f, "on:{}/{duration}", time.format("%Y-%m-%dT%H:%M")),
                TimeUri::DateDuration(date, duration) => write!(f, "on:{date}/{duration}"),
            }
        }
    }

    #[derive(PartialEq)]
    enum Interest {
        NotInterested,
        MaybeInterested,
        Interested(TimeUri),
    }

    impl Display for Interest {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                Interest::NotInterested => write!(f, "-"),
                Interest::MaybeInterested => write!(f, "?"),
                Interest::Interested(time_uri) => write!(f, "{time_uri}"),
            }
        }
    }

    pub fn find_events(input: &String, mut calendar: Calendar) -> Calendar {
        let parser = pulldown_cmark::Parser::new(input);

        // What's the level of nesting?
        let mut level = 0;

        // Can this be a description of an interesting calendar item?
        let mut interest = Interest::NotInterested;

        let mut text = String::new();

        for event in parser {
            // println!("{level} {interest} {event:?}");
            if let pulldown_cmark::Event::Start(_) = event {
                level += 1
            };
            if let pulldown_cmark::Event::End(_) = event {
                level -= 1;
                if interest == Interest::MaybeInterested {
                    // If we didn't pick an interest after first child, we loose it.
                    interest = Interest::NotInterested
                }
            };
            if level < 2 {
                if let Interest::Interested(uri) = interest {
                    let summary: String = text.lines().take(1).collect();
                    let summary = summary.trim().to_string();
                    let description = text
                        .lines()
                        .skip(1)
                        .skip_while(|line| line.trim().is_empty())
                        .collect::<Vec<&str>>()
                        .join("\n");
                    let item = match uri {
                        TimeUri::Time(time) => calendar::Item::Event {
                            start: time,
                            maybe_duration: None,
                            summary,
                            description,
                        },
                        TimeUri::Date(date) => calendar::Item::WholeDayEvent {
                            start: date,
                            maybe_duration: None,
                            summary,
                            description,
                        },
                        TimeUri::TimeDuration(time, duration) => calendar::Item::Event {
                            start: time,
                            maybe_duration: Some(duration),
                            summary,
                            description,
                        },
                        TimeUri::DateDuration(date, duration) => calendar::Item::WholeDayEvent {
                            start: date,
                            maybe_duration: Some(duration),
                            summary,
                            description,
                        }
                    };
                    calendar.add_item(item);
                };
                interest = Interest::NotInterested;
                text.clear();
            };

            match event {
                pulldown_cmark::Event::Start(pulldown_cmark::Tag::Item) => {
                    if level == 2 {
                        // Only top level list items can describe events.
                        interest = Interest::MaybeInterested
                    };
                }
                pulldown_cmark::Event::Start(pulldown_cmark::Tag::Link(_, href, _)) => {
                    if interest == Interest::MaybeInterested {
                        match parse_time_uri(href) {
                            Some(time) => {
                                interest = Interest::Interested(time);
                            }
                            None => {
                                ();
                            }
                        }
                    }
                }
                pulldown_cmark::Event::Text(content) => match interest {
                    Interest::Interested(time) => {
                        if format!("{time}") != content.to_string() {
                            text.push_str(&content)
                        }
                    }
                    Interest::MaybeInterested => interest = Interest::NotInterested,
                    _ => (),
                },
                pulldown_cmark::Event::SoftBreak => {
                    if let Interest::Interested(_) = interest {
                        text.push_str("\n")
                    }
                },
                pulldown_cmark::Event::Start(pulldown_cmark::Tag::Paragraph) => {
                    if let Interest::Interested(_) = interest {
                        text.push_str("\n\n")
                    }
                }
                _ => (),
            };
        }
        calendar
    }

    fn parse_time_uri(href: pulldown_cmark::CowStr) -> Option<TimeUri> {
        if let Ok(url) = Url::parse(&href) {
            if url.scheme() == "on" {
                if let [time_string, duration_string] = 
                    url.path()
                        .split('/')
                        .collect::<Vec<&str>>()[..] 
                {
                    if let (Ok(time), Ok(duration)) = (
                        chrono::NaiveDateTime::parse_from_str(time_string, "%Y-%m-%dT%H:%M"),
                        Duration::parse(duration_string),
                    )
                    {
                        return Some(TimeUri::TimeDuration(time, duration));
                    }    
                    if let (Ok(date), Ok(duration)) = (
                        chrono::NaiveDate::parse_from_str(time_string, "%Y-%m-%d"),
                        Duration::parse(duration_string),
                    )
                    {
                        return Some(TimeUri::DateDuration(date, duration));
                    }
                } 

                if let Ok(time) =
                    chrono::NaiveDateTime::parse_from_str(url.path(), "%Y-%m-%dT%H:%M")
                {
                    return Some(TimeUri::Time(time));
                }
                if let Ok(date) = chrono::NaiveDate::parse_from_str(url.path(), "%Y-%m-%d") {
                    return Some(TimeUri::Date(date));
                } else {
                    // TODO: Panic!
                    panic!("Invalid time URI: {href}");
                }
            } else {
                // Not a time URI
                None
            }
        } else {
            // Failed to parse the URL
            None
        }
    }
}
