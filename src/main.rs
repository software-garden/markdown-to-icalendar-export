use clap::Parser;
use md_to_ics::calendar;
use md_to_ics::converter;
use std::io;
use std::io::prelude::*;

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    /// Set the timezone, e.g. Europe/Amsterdam or UTC
    #[clap(short, long)]
    timezone: chrono_tz::Tz,
}

fn main() -> io::Result<()> {
    let args = Cli::parse();

    let calendar = calendar::Calendar {
        // TODO: Do not hardcode the calendar name. Get it from command line or front-matter.
        name: "Software Garden Agile Dashboard".to_string(),
        product: "software.garden/markdown-to-icalendar-export".to_string(),
        // TODO: Do not hardcode the timezone. Get it from command line or front-matter.
        timezone: args.timezone,
        items: vec![],
    };

    let mut stdin = std::io::stdin();
    let mut markdown = String::new();

    stdin.read_to_string(&mut markdown)?;

    let ics = converter::find_events(&markdown, calendar)
        .to_string()
        .replace("\n", "\r\n");

    print!("{}", ics);
    Ok(())
}
