include Makefile.d/defaults.mk

all: ## Run unit tests and build the program
all: test dist
.PHONY: all

test: ## Test the program
test: tests/unit tests/integration
.PHONY: test

tests/unit:
	cargo test
.PHONY: tests/unit

tests/integration:
	if cat tests/input.md \
	| cargo run -- \
	  --timezone=Europe/Amsterdam \
	| diff --color --unified tests/expected.ics -
	then
		echo "All good."
	else
		echo
		echo "Unexpected output. See the differences above."
	fi
.PHONY: tests/integration

dist: ## Build the program
dist: Cargo.lock
dist: $(shell find src/)
dist:
	$(error Not implemented)
	touch $@

install: ## Install the program in the ${prefix} directory
install: prefix ?= $(out)
install: dist
	test $(prefix)
	mkdir --parents $(prefix)
	$(error Not implemented)
.PHONY: install


### DEVELOPMENT

develop: ## Watch, rebuild and serve the content
develop: dependencies
develop:
	$(error Not implemented)
.PHONY: develop

clean: ## Remove all files set to be ignored by git
clean:
	git clean -dfX \
		--exclude='!.envrc.private'
.PHONY: clean

help: ## Print this help message (DEFAULT)
help: # TODO: Handle section headers in awk script
	@
	echo "Useage: make [ goals ]"
	echo
	echo "Available goals:"
	echo
	cat $(MAKEFILE_LIST) | awk -f Makefile.d/make-goals.awk
.PHONY: help

### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result:
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)
